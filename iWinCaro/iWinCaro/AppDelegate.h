//
//  AppDelegate.h
//  iWinCaro
//
//  Created by Wolverine on 17/06/2021.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
+ (AppDelegate *)sharedDelegate;

@property (strong, nonatomic)  UINavigationController *navVC;
@property (strong, nonatomic) UIWindow *window;
- (void)switchRootViewControllerAnimated:(UIViewController *)viewController animated:(BOOL)animated completion:(void (^)())completion;


@end

