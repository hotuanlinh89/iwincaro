//
//  UserModel.h
//  Jobal
//
//  Created by Wolverine on 6/18/20.
//  Copyright © 2020 hotuanlinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataModel.h"
@interface HomeDataModel : NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * subTitle;
@property (nonatomic, strong) NSString * messageLink;
@property (nonatomic, strong) NSMutableArray * guideDataArray;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

