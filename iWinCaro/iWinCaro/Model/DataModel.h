//
//  DataModel.h
//  iWinCaro
//
//  Created by Wolverine on 17/06/2021.
//

#import <Foundation/Foundation.h>


@interface DataModel : NSObject
@property (nonatomic, strong) NSString * heading;
@property (nonatomic, strong) NSString * headingLink;
-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

