//
//  UserModel.m
//  Jobal
//
//  Created by Wolverine on 6/18/20.
//  Copyright © 2020 hotuanlinh. All rights reserved.
//

#import "HomeDataModel.h"
@implementation HomeDataModel

- (id)init
{
    self = [super init];
    if (self) {
        self.title=@"title";
        self.subTitle=@"subTitle";
        self.messageLink=@"google.com";
        self.guideDataArray = [NSMutableArray new];
    }
    return self;
} 

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];

    for(NSDictionary* dict in dictionary[@"guideDatas"]){
    if(![dict isKindOfClass:[NSNull class]]){
        DataModel* dataModel = [[DataModel alloc] initWithDictionary:dict];
        [self.guideDataArray addObject:dataModel];
        }
    }

    if(![dictionary[@"title"] isKindOfClass:[NSNull class]]){
        self.title = dictionary[@"title"] ;
    }

    if(![dictionary[@"messageLink"] isKindOfClass:[NSNull class]]){
        self.messageLink = dictionary[@"messageLink"] ;
    }

    if(![dictionary[@"subTitle"] isKindOfClass:[NSNull class]]){
        self.subTitle = dictionary[@"subTitle"];
    }
    return self;
}
@end
