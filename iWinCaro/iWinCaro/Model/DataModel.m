//
//  DataModel.m
//  iWinCaro
//
//  Created by Wolverine on 17/06/2021.
//

#import "DataModel.h"

@implementation DataModel
- (id)init
{
    self = [super init];
    if (self) {
        self.heading=@"";
        self.headingLink=@"";
    }
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];

    if(![dictionary[@"heading"] isKindOfClass:[NSNull class]]){
        self.heading = dictionary[@"heading"] ;
    }
    if(![dictionary[@"headingLink"] isKindOfClass:[NSNull class]]){
        self.headingLink = dictionary[@"headingLink"];
    }
    return self;
}
@end
