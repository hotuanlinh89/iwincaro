//
//  AntsInsight
//
//  Created by Wolverine on 12/2/15.
//  Copyright © 2015 hotuanlinh. All rights reserved.

#import "BaseManager.h"
@interface BaseManager()
@end
@implementation BaseManager
+ (instancetype)sharedInstance
{
   // DLog(@"WARNING: Must always override +sharedInstance method with SINGLETON_MACRO in subclass of BaseManager");
    
    static dispatch_once_t pred;
    static id __singleton = nil;
    
    dispatch_once(&pred, ^{ __singleton = [[self alloc] init]; });
    return __singleton;
}
@end
