//
//  NSString+Extension.h
//  PhuNuNet
//
//  Created by MrJoJo on 7/11/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Extension)

-(NSString*)UnixTimeConvertTo;

- (NSString *)relativeDateStringFromStringDate;

-(NSDate*)getDateFromString:(NSString*)stringDate;

-(Boolean)checkIsImage;
@end
