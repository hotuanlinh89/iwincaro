//
//  MBProgressHUD+Extension.h
//  PhuNuNet
//
//  Created by MrJoJo on 7/11/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface MBProgressHUD(Extension)

+(void)showGlobalProgressHUDWithTitle:(NSString *)title WithAnimated:(BOOL)isAnimated;

+(void)dismissGlobalHUDWithAnimated:(BOOL)isAnimated;

+(void)showHudWithTitle:(NSString*)title WithView:(UIView*)view WithAnimated:(BOOL)isAnimated;

@end
