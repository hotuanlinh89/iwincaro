//
//  UIColor+Extension.h
//  PhuNuNet
//
//  Created by MrJoJo on 7/8/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor(Extension)

+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str;

@end
