//
//  UILabel+Extension.h
//  PhuNuNet
//
//  Created by MrJoJo on 8/1/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UILabel(Extension)

- (CGFloat)getLabelHeight;

@end
