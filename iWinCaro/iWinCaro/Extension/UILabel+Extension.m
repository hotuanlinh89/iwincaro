//
//  UILabel+Extension.m
//  PhuNuNet
//
//  Created by MrJoJo on 8/1/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel(Extension)

- (CGFloat)getLabelHeight
{
    CGSize constraint = CGSizeMake(self.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [self.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:self.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

@end
