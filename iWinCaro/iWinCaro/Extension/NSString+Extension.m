//
//  NSString+Extension.m
//  PhuNuNet
//
//  Created by MrJoJo on 7/11/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString(Extension)

-(NSString*)UnixTimeConvertTo
{
    double unixTimeStamp = [self doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    return [formatter stringFromDate:date];
}


- (NSString *)relativeDateStringFromStringDate
{
    
    NSDate *date = [self getDateFromString:self];
    
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear| NSCalendarUnitHour| NSCalendarUnitMinute;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    if (components.year > 0) {
        return [NSString stringWithFormat:@"%ld năm trước", (long)components.year];
    } else if (components.month > 0) {
        return [NSString stringWithFormat:@"%ld tháng trước", (long)components.month];
    } else if (components.weekOfYear > 0) {
        return [NSString stringWithFormat:@"%ld tuần trước", (long)components.weekOfYear];
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld ngày trước", (long)components.day];
        } else {
            return @"Hôm qua";
        }
    } else if (components.hour >0) {
        return [NSString stringWithFormat:@"%ld giờ trước", (long)components.hour];
    }
    
    else if (components.minute >0) {
        return [NSString stringWithFormat:@"%ld phút trước", (long)components.minute];
    }
    else{
        return @"Vừa xong";
    }
}

-(NSDate*)getDateFromString:(NSString*)stringDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    NSDate *dateResult = [dateFormatter dateFromString:stringDate];
    
    return dateResult;
}

-(Boolean)checkIsImage
{
    NSString * lastString = [self componentsSeparatedByString:@"."].lastObject;
    
    if ([lastString isEqualToString:@"jpg"] || [lastString isEqualToString:@"png"] || [lastString isEqualToString:@"JPG"] || [lastString isEqualToString:@"PNG"] || [lastString isEqualToString:@"jpeg"] || [lastString isEqualToString:@"gif"])
    {
        return true;
    }
    else
    {
        return false;
    }
}

@end
