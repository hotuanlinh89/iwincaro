//
//  WebViewViewController.m
//  iWinCaro
//
//  Created by Wolverine on 17/06/2021.
//

#import "WebViewViewController.h"
#import <WebKit/WebKit.h>
@interface WebViewViewController ()<WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet WKWebView *myWebview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *myIndicator;
@property (weak, nonatomic) IBOutlet UIButton *goBackButton;
@property (weak, nonatomic) IBOutlet UIButton *goForwardButton;
@property (weak, nonatomic) IBOutlet UIView *buttonView;

- (IBAction)tapOnGobackButton:(id)sender;
- (IBAction)tapOnGoforwardButton:(id)sender;
- (IBAction)tapOnReloadButton:(id)sender;
@end

@implementation WebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title= self.stringUrl;
    [self setCustomNavbarLeftButtonWithImageName:@"back_no_shadow" selector:@selector(back)];
    [self loadWebView];
    self.buttonView.layer.masksToBounds = NO;
    self.buttonView.layer.shadowOffset = CGSizeMake(-5, 2);
    self.buttonView.layer.shadowOpacity = 0.4;
}
-(void)checkEnableButton{
    [self.myWebview canGoBack]  ? (self.goBackButton.enabled = true) : (self.goBackButton.enabled = false);
    [self.myWebview canGoForward]  ? (self.goForwardButton.enabled = true) : (self.goForwardButton.enabled = false);
}

-(void)loadWebView{
    self.myWebview.navigationDelegate = self;
    NSURL *url = [NSURL URLWithString:self.stringUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebview loadRequest:request];
    [self checkEnableButton];
    [self.myWebview addObserver:self forKeyPath:@"canGoBack" options:NSKeyValueObservingOptionNew context:nil];
    [self.myWebview addObserver:self forKeyPath:@"canGoForward" options:NSKeyValueObservingOptionNew context:nil];
    
}

-(void)observeValueForKeyPath:(NSString*)keyPath
                     ofObject:(id)object
                       change:(NSDictionary*)change
                      context:(void*)context {
    if ([keyPath isEqual:@"canGoBack"] ||[keyPath isEqual:@"canGoForward"] ) {
        [self checkEnableButton];
    }
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [self.myIndicator stopAnimating];
}
- (IBAction)tapOnReloadButton:(id)sender {
    [self.myWebview reload];
}

- (IBAction)tapOnGoforwardButton:(id)sender {
    if ([self.myWebview canGoForward]) {
        [self.myWebview goForward];
    }
}

- (IBAction)tapOnGobackButton:(id)sender {
    if ([self.myWebview canGoBack]) {
        [self.myWebview stopLoading];
        [self.myWebview goBack];
    }
}
@end
