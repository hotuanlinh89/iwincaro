//
//  WebViewViewController.h
//  iWinCaro
//
//  Created by Wolverine on 17/06/2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebViewViewController : BaseViewController
@property (strong, nonatomic) NSString* stringUrl;

@end

NS_ASSUME_NONNULL_END
