//
//  JSListJobTableViewCell.m
//  Jobal
//
//  Created by Wolverine on 6/26/20.
//  Copyright © 2020 hotuanlinh. All rights reserved.
//

#import "JSListHomeDataTableViewCell.h"

@implementation JSListHomeDataTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
   // self.jobImage.layer.borderColor = [[UIColor lightGrayColor] CGColor];
   // self.jobImage.layer.borderWidth=1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self == nil)
        return self;
    return self;
}


- (NSString*)reuseIdentifier
{
    return @"JSListHomeDataTableViewCell";
}


+ (NSString *)identifider;{
    return NSStringFromClass([self class]);
}
+ (id)cellNib{
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"JSListHomeDataTableViewCell" owner:nil options:nil];
    
    JSListHomeDataTableViewCell *autoCell = (JSListHomeDataTableViewCell *)[views objectAtIndex:0];
    return autoCell;
}

@end
