//
//  JSListJobTableViewCell.h
//  Jobal
//
//  Created by Wolverine on 6/26/20.
//  Copyright © 2020 hotuanlinh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JSListHomeDataTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *jobImage;
@property (weak, nonatomic) IBOutlet UILabel *jobContent;

+ (NSString *)identifider;
+ (id)cellNib;
@end

NS_ASSUME_NONNULL_END
