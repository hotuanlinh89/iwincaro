//
//  HomeViewController.m
//  iWinCaro
//
//  Created by Wolverine on 17/06/2021.
//

#import "HomeViewController.h"
#import "JSListHomeDataTableViewCell.h"
#import "WebViewViewController.h"
#import "GameController.h"
#import <FirebaseDatabase/FirebaseDatabase.h>

@interface HomeViewController()<UITableViewDelegate,UITableViewDataSource>{
    HomeDataModel* dataModel;
}
@property (weak, nonatomic) IBOutlet UITableView *listDataTableview;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *myIndicator;
@property (weak, nonatomic) IBOutlet UIButton *playNowButton;
- (IBAction)tapOnPlayGameButton:(id)sender ;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.playNowButton.layer.cornerRadius= 5;
    [self getDataFireBase];
}

-(void)goToMessage{
    WebViewViewController* vc =[[WebViewViewController alloc] initWithNib];
    vc.stringUrl = dataModel.messageLink;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)getDataFireBase{
    FIRDatabaseReference *rootRef= [[FIRDatabase database] referenceFromURL:@"https://iostrafficsite-default-rtdb.firebaseio.com/"];
        [rootRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
        {
            [self.myIndicator stopAnimating];
            if (snapshot.exists)
            {
                [self setDataWithJson:snapshot.value[@"data"]];
            }
        }];
}
-(void)setDataWithJson:(NSDictionary*)dict{
    dataModel = [[HomeDataModel alloc] initWithDictionary:dict];
    [self.listDataTableview reloadData];
    self.title = dataModel.title;
    self.subTitleLabel.text = dataModel.subTitle;
    if(dataModel.messageLink.length > 0){
        [self setCustomNavbarRightButtonWithImageName:@"message_white" selector:@selector(goToMessage)];
    }
}
#pragma mark - tableview -
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataModel.guideDataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSListHomeDataTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[JSListHomeDataTableViewCell identifider]];
    if(!cell)
    {
        cell = [JSListHomeDataTableViewCell cellNib];
    }
    
    DataModel* model =[dataModel.guideDataArray objectAtIndex:indexPath.row];
    
    cell.jobContent.text= model.heading;
    if (indexPath.row % 2 == 0 ) {
        cell.jobImage.image = [UIImage imageNamed:@"guide2"];
    }else{
        cell.jobImage.image = [UIImage imageNamed:@"guide1"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WebViewViewController* vc =[[WebViewViewController alloc] initWithNib];
    DataModel* model =[dataModel.guideDataArray objectAtIndex:indexPath.row];
    vc.stringUrl = model.headingLink;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - actions -
- (IBAction)tapOnPlayGameButton:(id)sender {
    GameController *vc = [[GameController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
