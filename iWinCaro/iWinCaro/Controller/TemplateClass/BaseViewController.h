//
//  BaseViewController.h
//  PhuNuNet
//
//  Created by MrJoJo on 7/11/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
@property (nonatomic, strong) UITextField *activeTextField;
@property (nonatomic, assign) BOOL previousVCIsHome;



- (UIViewController*)initWithNib;

/*
 * Obtain a class instance object from a string class name
 */
- (UIViewController*)getViewControllerWithClassName:(NSString*)theClassName;

/*
 * Use a standard 'Back' button in navbar instead of title of previous view controller
 */
- (void)useStandardNavbarBackButton;

/*
 * Use a custom button title for left/right navbar button, with standard look & feel
 */
- (UIBarButtonItem *)setCustomNavbarLeftButtonTitle:(NSString*)title selector:(SEL)selector;
- (UIBarButtonItem *)setCustomNavbarRightButtonTitle:(NSString*)title selector:(SEL)selector;
- (UIBarButtonItem *)setCustomNavbarLeftButtonWithImageName:(NSString*)imageName selector:(SEL)selector;
- (UIBarButtonItem *)setCustomNavbarRightButtonWithImageName:(NSString*)imageName selector:(SEL)selector;


-(float)messageSizeAbout:(NSString*)message withFont:(UIFont*)fontText andTextWidth:(CGFloat)widthText andHeightAdd:(CGFloat)heightAddMore;

- (NSString *)relativeDateStringForDate:(NSTimeInterval)dateInterval;



-(NSString *) stringByStrippingHTML :(NSString*)inputString ;



-(NSDate*)getDateFromString:(NSString*)stringDate;

-(NSString*)getStringFromDate:(NSDate*)date;

-(BOOL)compareDateFrom:(NSDate*)dateFrom withdateTo:(NSDate*)dateTo;

-(NSString*)getDateAgoWithDateNumber:(NSInteger)number;

-(NSString*)getDateAgoWithHourNumber:(NSInteger)number;

- (NSString *)timeFormatted:(long)totalSeconds;

-(void)setTitleLeftWithString:(NSString*)string;

-(void)showHudWithTitle:(NSString*)title;

-(void)hideHud;

-(void)back;
@end
