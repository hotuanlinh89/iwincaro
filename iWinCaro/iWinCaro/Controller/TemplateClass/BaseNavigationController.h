//
//  BaseNavigationController.h
//  AntsInsight
//
//  Created by Wolverine on 12/2/15.
//  Copyright © 2015 hotuanlinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController
- (UIViewController*)initWithNib;
@end
