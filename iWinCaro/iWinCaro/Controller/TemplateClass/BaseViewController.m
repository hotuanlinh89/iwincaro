//
//  BaseViewController.m
//  PhuNuNet
//
//  Created by MrJoJo on 7/11/16.
//  Copyright © 2016 ANTS. All rights reserved.
//

#import "BaseViewController.h"
#import "UIView+Additions.h"

@interface BaseViewController ()@property (nonatomic, assign) BOOL isKeyboardShowing;
@end

@implementation BaseViewController


- (UIViewController*)initWithNib
{
    NSString *className = NSStringFromClass([self class]);
    self = [self initWithNibName:className bundle:nil];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self == nil)
        return self;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
-(void)didReceiveMemoryWarning{
    
  
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
//    DLog(@"Warning: Do not put any code in viewDidUnload. Deprecated since iOS 6.0");
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    
    if (self.parentViewController != nil) {
        [super viewDidDisappear:animated];
        return;
    }
    
    if (self.previousVCIsHome == NO) {
        [super viewDidDisappear:animated];
        return;
    }
    
    // [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Rotations

//iOS 5.0 Rotations
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

//For view controller container contaiment, if any
- (BOOL)shouldAutomaticallyForwardRotationMethods
{
    return YES;
}

//iOS 6.0 Rotations
- (BOOL)shouldAutorotate
{
    return YES;
}


#pragma mark - UI Helpers

- (UIViewController*)getViewControllerWithClassName:(NSString*)theClassName
{
    if ([theClassName length] <= 0)
        return nil;
    
    //Dynamically load the class
    Class theClass = NSClassFromString(theClassName);
    if (theClass == nil)
        return nil;
    
    NSObject* myViewController = [[theClass alloc] init];
    if (myViewController == nil)
        return nil;
    if ([myViewController isMemberOfClass:[UIViewController class]])
        return nil;
    
    return (UIViewController*)myViewController;
}

/*
 * Use a standard 'Back' button in navbar instead of title of previous view controller
 */
- (void)useStandardNavbarBackButton
{
    if (self.navigationController == nil)
        return;
    
    //Clear the current button, if any
    self.navigationItem.leftBarButtonItem = nil;
    
    SEL action = nil;
    if ([self respondsToSelector:@selector(onBtnBack:)])
        action = @selector(onBtnBack:);
    
    
    //Zoro Update image of back button V3
    [self setCustomNavbarLeftButtonWithImageName:@"navbarProfileBack" selector:@selector(onBtnBack:)];
}

- (UIBarButtonItem *)setCustomNavbarLeftButtonTitle:(NSString*)title selector:(SEL)selector
{
    if (self.navigationController == nil)
        return nil;
    if ([title length] <= 0)
        return nil;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(title, title)
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:selector];
    self.navigationItem.leftBarButtonItem = barButton;
    return barButton;
}

- (UIBarButtonItem *)setCustomNavbarRightButtonTitle:(NSString*)title selector:(SEL)selector
{
    if (self.navigationController == nil)
        return nil;
    if ([title length] <= 0)
        return nil;
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(title, title)
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:selector];
    barButton.tintColor = [UIColor whiteColor];

    [barButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Roboto-Medium" size:15.0],
      NSFontAttributeName,
      nil]
                             forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = barButton;
    return barButton;
}

- (UIBarButtonItem *)setCustomNavbarLeftButtonWithImageName:(NSString*)imageName selector:(SEL)selector
{
    if (self.navigationController == nil)
        return nil;
    if ([imageName length] <= 0)
        return nil;
    
    UIBarButtonItem *barButton = [self navbarButtonItemWithTarget:self
                                                           action:selector
                                                       imageNamed:imageName];
    self.navigationItem.leftBarButtonItem = barButton;
    barButton.tintColor = [UIColor whiteColor];

    return barButton;
}

- (UIBarButtonItem *)setCustomNavbarRightButtonWithImageName:(NSString*)imageName selector:(SEL)selector
{
    if (self.navigationController == nil)
        return nil;
    if ([imageName length] <= 0)
        return nil;
    
    UIBarButtonItem *barButton = [self navbarButtonItemWithTarget:self
                                                           action:selector
                                                       imageNamed:imageName];
    
    
    self.navigationItem.rightBarButtonItem = barButton;
    return barButton;
}


- (UIBarButtonItem *)navbarButtonItemWithTarget:(id)target action:(SEL)action imageNamed:(NSString *)name
{
    UIImage *barButtonItemImage = [UIImage imageNamed:name];
    
    UIButton *barButtonItemButton = [UIButton buttonWithType:UIButtonTypeCustom];
    barButtonItemButton.backgroundColor = [UIColor clearColor];
    barButtonItemButton.size = barButtonItemImage.size;
    [barButtonItemButton setImage:barButtonItemImage forState:UIControlStateNormal];
    
    if (target && action)
        [barButtonItemButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc] initWithCustomView:barButtonItemButton];
}
#pragma mark - Actions

- (IBAction)onBtnBack:(id)sender
{
    if (self.navigationController == nil) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.activeTextField = nil;
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Navbar animation

- (void)hideTopAndBottomBars
{
    BOOL hasTabbar = self.tabBarController != nil;
    CGRect appFrame = [[UIScreen mainScreen] bounds];
    CGRect navbarFrame = self.navigationController.navigationBar.bounds;
    navbarFrame.origin.y = -navbarFrame.size.height;
    
    if (hasTabbar)
        appFrame.size.height += 54;
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.view.window.frame = CGRectMake(0, -navbarFrame.size.height, appFrame.size.width, appFrame.size.height+navbarFrame.size.height);
    } completion:nil];
    
    [UIView animateWithDuration:0.30 delay:0.15 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.navigationController.navigationBar.frame = navbarFrame;   //push up by 20
    } completion:nil];
}

- (void)showTopAndBottomBars
{
    if ([UIApplication sharedApplication].statusBarHidden)
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
    CGRect navbarFrame = self.navigationController.navigationBar.bounds;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.navigationController.navigationBar.frame = navbarFrame;  //push down by 20
    } completion:nil];
    
    [UIView animateWithDuration:0.25 delay:0.15 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.view.window.frame = appFrame;
    } completion:nil];
}




-(void)setTitleLeftWithString:(NSString*)string{
    
    UILabel* lbNavTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    lbNavTitle.textAlignment = NSTextAlignmentLeft;
    lbNavTitle.text = string ;
    lbNavTitle.textColor=[UIColor whiteColor];
    [lbNavTitle setFont:[UIFont fontWithName:@"Roboto-Bold" size:17.0]];
    self.navigationItem.titleView = lbNavTitle;
}

#pragma mark - actions -
-(void)back{
    
    if([self.navigationController.viewControllers count] == 1)  {
          [self dismissViewControllerAnimated:YES completion:nil];
       }else{
           [self.navigationController popViewControllerAnimated:YES];
       }}
@end
