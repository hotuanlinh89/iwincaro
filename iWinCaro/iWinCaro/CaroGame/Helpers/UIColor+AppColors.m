//
//  UIColor+AppColors.m
//  TicTacToeGame
//
//  Created by Bimal Thapa on 12/13/17.
//  Copyright © 2017 Bimal Thapa. All rights reserved.
//

#import "UIColor+AppColors.h"

@implementation UIColor (AppColors)

+ (UIColor *) lightBlueColor {
    
    return [UIColor colorWithRed:0.0/255.0 green:153.0/255.0 blue:255.0/255.0 alpha:1.0];
}

+(UIColor *)buttonColor {
    
    return DEFAULT_COLOR_APP;
}

@end
