//
//  AppDelegate.m
//  iWinCaro
//
//  Created by Wolverine on 17/06/2021.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
@import Firebase;

@interface AppDelegate ()

@end

@implementation AppDelegate
+ (AppDelegate *)sharedDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    [self.window makeKeyAndVisible];
    [self showHomeVC];

    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:103.0/255.0 green:190.0/255.0 blue:72.0/255.0 alpha:1.0]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [FIRApp configure];
    return YES;
}




- (void)switchRootViewControllerAnimated:(UIViewController *)viewController animated:(BOOL)animated completion:(void (^)(void))completion
{
    if (!animated) {
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
        
        if (completion) completion();
        return;
    }
    
    [UIView transitionWithView:self.window duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        BOOL oldState = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        self.window.rootViewController = viewController;
        
        [UIView setAnimationsEnabled:oldState];
    } completion:^(BOOL finished) {
        if (completion) completion();
    }];
}

-(void)showHomeVC{
    HomeViewController *vc = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    BaseNavigationController *navVC = [[BaseNavigationController alloc] initWithRootViewController:vc];
    if (@available(iOS 13, *))
    {
        UINavigationBarAppearance *navBar = [[UINavigationBarAppearance alloc] init];
        navBar.backgroundColor = DEFAULT_COLOR_APP;
        [navBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                            [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
        navVC.navigationBar.standardAppearance = navBar;
        navVC.navigationBar.scrollEdgeAppearance = navBar;
    }
    [self switchRootViewControllerAnimated:navVC animated:NO completion:nil];
}
@end
